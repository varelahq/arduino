/**
 * The MySensors Arduino library handles the wireless radio link and protocol
 * between your home built sensors/actuators and HA controller of choice.
 * The sensors forms a self healing radio network with optional repeaters. Each
 * repeater and gateway builds a routing tables in EEPROM which keeps track of the
 * network topology allowing messages to be routed to nodes.
 *
 * Created by Henrik Ekblad <henrik.ekblad@mysensors.org>
 * Copyright (C) 2013-2015 Sensnology AB
 * Full contributor list: https://github.com/mysensors/Arduino/graphs/contributors
 *
 * Documentation: http://www.mysensors.org
 * Support Forum: http://forum.mysensors.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 *******************************
 *
 * REVISION HISTORY
 * Version 1.0 - Cristian Varela
 *
 * DESCRIPTION
 * The TMP36 (http://ctms.engin.umich.edu/CTMS/Content/Activities/TMP35_36_37.pdf) is the 
 * temperature sensor included in the Arduino started kit. This Sketch is a custom version
 * using MySensors libraries.
 *
 */

/* Comment MY_NODE_ID if you want to let the GW assign a NODE ID automatically, unless
 * the GW is MQTT in which case you have to define the NODE ID manually.
 */
#define MY_NODE_ID 18

// Enable debug prints
#define MY_DEBUG

// Enable and select radio type attached 
#define MY_RADIO_NRF24
//#define MY_RADIO_RFM69
//#define MY_RS485

#include <MySensors.h>

#define CHILD_ID 1 //Temperature Sensor ID - See Serial API for more info 
                   // https://www.mysensors.org/download/serial_api_20

#define TMP36_DATA_PIN A0 //Analog Pin where Sensor is connected
#define PERM_TEMP_ADJ -1.5  //Adjustment of the temperature sensor

/*Force a SEND every MAX_READ_COUNTS even if the Temperature 
hasn't changed.*/
#define MAX_READ_COUNTS 15 

/*Miliseconds between readings of the sensor*/
#define MILLIS_BETWEEN_READS 60000

float prevTemp = 0.0;
int currentReadCounts = 0; 

MyMessage msg(CHILD_ID, V_TEMP);

void setup(){
  currentReadCounts = MAX_READ_COUNTS;
}

void presentation()
{
  present(CHILD_ID, S_TEMP);
}

float getTMP36Temp(){
  /*
   * Function to read the voltage from the sensore
   * and convert it to Celsius
   */
  int sensorVal = analogRead(TMP36_DATA_PIN); //Reading from analog input 0 --> 0v, 1024 --> 5v
  float voltage = (sensorVal / 1024.0) * 5.0;  // Convert [0,1024) range of Arduino AnalogInputs into Voltage
  /*According to documentation the TMP36 is linearly proportial to Celsius so 0.075v=25C --> 0.080=30c
  */
  float temperature = (voltage - 0.5) * 100.0;  
  float adj_temp = temperature + PERM_TEMP_ADJ;
  return adj_temp;
}

void loop() {
  float temperature = getTMP36Temp();
  currentReadCounts = currentReadCounts + 1;
  // Send if MAX_READ_COUNTS have occurred or temperature has changed
  if (currentReadCounts >= MAX_READ_COUNTS || prevTemp != temperature) {
    send(msg.set(temperature, V_TEMP));  
    currentReadCounts = 0;
  }
  prevTemp = temperature; 
  wait(MILLIS_BETWEEN_READS);
}
